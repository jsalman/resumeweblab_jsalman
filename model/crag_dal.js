var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM crag;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getSimple = function(cr_num, callback) {
    var query = 'SELECT cr.cr_num, cr.crag_name FROM crag cr WHERE cr.cr_num = ?;';

    var queryData = [cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(cr_num, callback) {
    var query = 'SELECT cr.*, ca.area_name FROM crag cr ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'WHERE cr.cr_num = ?;';

    var queryData = [cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getSport = function(cr_num, callback) {
    var query = 'SELECT s.s_num, s.s_name, s.s_grade FROM sport_lead s WHERE s.cr_num = ?;';

    var queryData = [cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getBoulder = function(cr_num, callback) {
    var query = 'SELECT b.b_num, b.b_name, b.b_grade FROM boulder b WHERE b.cr_num = ?;';

    var queryData = [cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getTrad = function(cr_num, callback) {
    var query = 'SELECT t.t_num, t.t_name, t.t_grade FROM trad_lead t WHERE t.cr_num = ?;';

    var queryData = [cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO crag (ca_num, crag_name, location, cr_cli_type) VALUES ?;';

    var queryData = [];

    queryData.push([params.ca_num, params.crag_name, params.location, params.cr_cli_type]);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE crag SET crag_name = ?, location = ?, cr_cli_type = ? WHERE cr_num = ?;';

    var queryData = [params.crag_name, params.location, params.cr_cli_type, params.cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(cr_num, callback) {
    var query = 'DELETE FROM crag WHERE cr_num = ?;';

    var queryData = [cr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};