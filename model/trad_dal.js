var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM trad_lead;';

    connection.query(query, function(err, callback) {
        callback(err, result);
    });
};

exports.getById = function(t_num, callback) {
    var query = 'SELECT t.*, cr.crag_name, ca.area_name FROM trad_lead t ' +
        'LEFT JOIN crag cr ON cr.cr_num = t.cr_num ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'WHERE t.t_num = ?;';

    var queryData = [t_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO trad_lead (cr_num, t_name, t_grade, t_rating, t_summary, descent, safety, num_pitches, t_risk) VALUES ?;';

    var queryData = [];

    queryData.push([params.cr_num, params.t_name, params.t_grade, params.t_rating, params.t_summary, params.descent, params.safety, params.num_pitches, params.t_risk]);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(t_num, callback) {
    var query = 'DELETE FROM trad_lead WHERE t_num = ?;';

    var queryData = [t_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'Update trad_lead SET t_name = ?, t_grade = ?, t_rating = ?, t_summary = ?, descent = ?, safety = ?, num_pitches = ?, t_risk = ? WHERE t_num = ?;';

    var queryData = [params.t_name, params.t_grade, params.t_rating, params.t_summary, params.descent, params.safety, params.num_pitches, params.t_risk];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
